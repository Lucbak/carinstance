from car import Car

new_car = Car(int(input('Enter the number of passengers')), int(input('Enter the car massu')),
              int(input('Enter the number of gears')),
              int(input('Enter the mass of passenger')))

if new_car.car_mass > 2000:
    raise ValueError("The car weight's to high")

if new_car.pax_count > 5:
    raise ValueError("Number of passengers is too high")
elif new_car.pax_count < 1:
    raise ValueError("Number of passengers is too small")

print(new_car.pax_count, new_car.car_mass, new_car.gear_count, new_car.person_weight)
print(new_car.total_mass())
